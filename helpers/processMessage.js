const API_AI_TOKEN = "a3e31f19b4194648bf3376899b3dfb4b";
const apiAiClient = require("apiai")(API_AI_TOKEN);
const FACEBOOK_ACCESS_TOKEN = "EAADExV4Q0ysBAMuM2Lsn8GvCDS4ifu3ZASR71KysWinfZCSJg4FEzTHPHZCaL44uM1Xv9lpBAZBcLnniXg9aHJzcrd4YvHuMLJ6ZCpv9Wb6a0FMTfIgubKvqYw4l6Lg1b9WGYBqxf1IfdifjtHMMdc8Sh1ZBj8aFDQRYobRnXaMwZDZD";
const request = require("request");
const sendTextMessage = (senderId, text) => {
  request({
    url: "https://graph.facebook.com/v2.6/me/messages",
    qs: {
      access_token: FACEBOOK_ACCESS_TOKEN
    },
    method: "POST",
    json: {
      recipient: {
        id: senderId
      },
      message: {
        text
      },
    }
  });
};
module.exports = (event) => {
  const senderId = event.sender.id;
  const message = event.message.text;
  console.log("Sender: "  + JSON.stringify(event.sender, null, 2))
  const apiaiSession = apiAiClient.textRequest(message, {
    sessionId: "crowdbotics_bot"
  });
  apiaiSession.on("response", (response) => {
    console.log(JSON.stringify(response.result,null,2));
    const result = response.result.fulfillment.speech;
    sendTextMessage(senderId, result);
  });
  apiaiSession.on("error", error => console.log(error));
  apiaiSession.end();
};